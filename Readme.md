# Thunder Assist 2


## Packaging

Put **src** and **manifest.json** into a zip archive with *.xpi* extension.

## lama.cpp command

```bash
./server -m ./models/openhermes-2-mistral-7b.Q5_K_M.gguf -c 1024 -ngl 20
```

`ngl` to load 20 layers on the GPU, increase if possible.
