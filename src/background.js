

async function processHtmlForGrammar(htmlBody) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(htmlBody, 'text/html');
    const body = doc.body;

    await traverseAndCorrect(body);
    removeProcessedNodeClass(body);

    return body.innerHTML;
}

function replaceTextNodeWithHtml(textNode, htmlContent) {
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = htmlContent;
    tempDiv.classList.add('processed-node');
    textNode.parentNode.classList.add('processed-node')

    // Insert each child of the tempDiv before the original text node
    while (tempDiv.firstChild) {
        const child = tempDiv.firstChild;



        // Only add 'processed-node' class to element nodes
        if (child.nodeType === Node.ELEMENT_NODE) {
            child.classList.add('processed-node');
        }

        textNode.parentNode.insertBefore(child, textNode);
    }

    // Remove the original text node
    textNode.parentNode.removeChild(textNode);
}




async function traverseAndCorrect(node) {

    if (node.nodeType === Node.TEXT_NODE && !node.parentNode.classList.contains('processed-node') && !node.parentNode.classList.contains('moz-signature')) {
        const originalText = node.nodeValue;

        const correctedText = await grammar_check(originalText);

        console.log("Corrected: " + correctedText)
        // var diffText = myersWordDiff(originalText, correctedText);
        var diffFormat = myersWordDiffWithFormat(originalText, correctedText);
        console.log(diffFormat)
        // console.log(diffText)
        const newHtmlContent = createColoredHtmlContent(diffFormat);
        replaceTextNodeWithHtml(node, newHtmlContent);
    } else if (node.nodeType === Node.ELEMENT_NODE && !node.classList.contains('processed-node')) {
        for (const child of node.childNodes) {
            await traverseAndCorrect(child);
        }
    }
}

function removeProcessedNodeClass(node) {
    if (node.nodeType === Node.ELEMENT_NODE) {
        node.classList.remove('processed-node');
        
        node.childNodes.forEach(childNode => {
            removeProcessedNodeClass(childNode);
        });
    }
}


async function injectCSS(tabId, cssFile) {
    await browser.tabs.insertCSS(tabId, {
        file: cssFile
    });
}

async function injectJS(tabId, jsFile) {
    await browser.tabs.executeScript(tabId, {
        file: jsFile
    })
}



function createColoredHtmlContent(diffs) {
    let htmlContent = '';

    diffs.forEach(diff => {
        const { text, correction } = diff;

        if (correction !== undefined) {
            // When there's a correction, create toggle buttons with both alternatives
            htmlContent += `<span class="toggle-container">` +
                           `<button id="or" class="toggle-option original">${text}</button>` +
                           `<button id="co" class="toggle-option corrected">${correction}</button>` +
                           `</span>`;
        } else {

            htmlContent += `${text}`;
        }
    });

    return htmlContent;
}

function showOriginalText(element) {
    // Logic to show original text
    // You may need to toggle classes or styles to highlight the selected option

    console.log("Original:")
}

function showCorrectedText(element) {
    // Logic to show corrected text
    // You may need to toggle classes or styles to highlight the selected option
    console.log("Corrected: ")
}





async function updateComposeWindowContent(tabId, newContent) {
    await browser.compose.setComposeDetails(tabId, { body: newContent });
}



async function getServerEndpoint() {
    var options = await browser.storage.sync.get("endpoint");
    return options.endpoint;
}

async function send_prompt(prompt, n_predict = 2048, temp = 0.1) {
    

    const endpoint = await getServerEndpoint();

    console.log(endpoint)
    let response = await fetch(endpoint, {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            prompt,
            n_predict: n_predict,
            temperature: temp,
            top_k: 10,
        }),
    }).catch((err) => {
        console.error(err)
    })
    console.log(response)
    let ans = await response.json()
    console.log(ans)

    return ans.content;
    
}

async function grammar_check(text) {

    // var prompt = "You are a multilingual grammar and spelling correction system. Upon receiving a text input, your task is to produce an accurately corrected text, ensuring it is free of any grammatical, spelling, or punctuation errors. The output must be in the SAME LANGUAGE as the input. No explanation, You only output the corrected text."
    // var prompt = "Correct grammatical errors in the following text in the original language and ONLY output the corrected version, then STOP."
    // + "\nText: " + text + "\nCorrection: ";

    var prompt = "[INST]You are an advanced french/english, grammar and spelling correction system. Upon receiving a text input, your task is to produce a faithful text in the same language, ensuring it is free of any grammatical, spelling, punctuation or any errors. You must stay close to the input text. You output a JSON object with the input language (key: language), then the corrected text in that language (key: corrected), no explanations, nothing but the JSON."
    + "\nInput text:" + text + "[/INST]";
    
    console.log("Send prompt:\n" + prompt);
    var ai_answer = await send_prompt(prompt);
    console.log(ai_answer)

    // Parse the JSON string into an object
    let jsonObject = JSON.parse(ai_answer);

    // Access the 'corrected' property
    let correctedValue = jsonObject.corrected;


    return correctedValue;
    
}


// Main function
browser.composeAction.onClicked.addListener(async (tab) => {

    await injectCSS(tab.id, './src/ui.css');

    const composeDetails = await browser.compose.getComposeDetails(tab.id);
    const htmlBody = composeDetails.body;

    const newHtmlContent = await processHtmlForGrammar(htmlBody);
    await updateComposeWindowContent(tab.id, newHtmlContent);

    await injectJS(tab.id, './src/user.js');

    // const updatedComposeDetails = await browser.compose.getComposeDetails(tab.id);

    // const buttons = updatedComposeDetails.querySelectorAll('.original');
    // buttons.forEach(button => {
    //     button.addEventListener('click', function() {
    //         showOriginalText(this);
    //     });
    // });
});
