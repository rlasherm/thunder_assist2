function myersWordDiff(original, modified) {
    const origWords = original.split(/\s+/);
    const modWords = modified.split(/\s+/);
    const n = origWords.length;
    const m = modWords.length;
    const max = n + m;
    const v = new Array(2 * max + 1);
    const trace = [];

    v[max + 1] = 0;
    for (let d = 0; d <= max; d++) {
        for (let k = -d; k <= d; k += 2) {
            let x;
            if (k === -d || (k !== d && v[max + k - 1] < v[max + k + 1])) {
                x = v[max + k + 1];
            } else {
                x = v[max + k - 1] + 1;
            }

            let y = x - k;
            while (x < n && y < m && origWords[x] === modWords[y]) {
                x++; y++;
            }

            v[max + k] = x;
            if (x >= n && y >= m) {
                trace.push(v.slice());
                return buildWordDiff(trace, origWords, modWords, max);
            }
        }
        trace.push(v.slice());
    }
}

function buildWordDiff(trace, origWords, modWords, max) {
    let x = origWords.length;
    let y = modWords.length;
    const diff = [];

    for (let d = trace.length - 1; d >= 0; d--) {
        const v = trace[d];
        let k = x - y;
        let prevK = k;

        if (k === -d || (k !== d && v[max + k - 1] < v[max + k + 1])) {
            prevK++;
        } else {
            prevK--;
        }

        const prevX = v[max + prevK];
        const prevY = prevX - prevK;

        while (x > prevX && y > prevY) {
            diff.unshift({
                text: origWords[x - 1],
                added: false,
                removed: false
            });
            x--; y--;
        }

        if (d > 0) {
            if (x > prevX) {
                diff.unshift({
                    text: origWords[x - 1],
                    added: false,
                    removed: true
                });
                x--;
            } else if (y > prevY) {
                diff.unshift({
                    text: modWords[y - 1],
                    added: true,
                    removed: false
                });
                y--;
            }
        }
    }

    return diff;
}

function myersWordDiffWithFormat(original, modified) {
    const origWords = original.split(/\s+/);
    const modWords = modified.split(/\s+/);
    const diff = myersWordDiff(original, modified);
    const formattedDiff = [];

    diff.forEach(item => {
        if (!item.removed && !item.added) {
            // Common word
            if (formattedDiff.length > 0 && formattedDiff[formattedDiff.length - 1].correction === undefined) {
                formattedDiff[formattedDiff.length - 1].text += ' ' + item.text;
            } else {
                formattedDiff.push({ text: item.text });
            }
        } else if (item.removed || item.added) {
            const lastItem = formattedDiff[formattedDiff.length - 1];
            if (lastItem && lastItem.correction !== undefined) {
                lastItem.text += (lastItem.text ? ' ' : '') + (item.removed ? item.text : '');
                lastItem.correction += (lastItem.correction ? ' ' : '') + (item.added ? item.text : '');
            } else {
                formattedDiff.push({ text: item.removed ? item.text : '', correction: item.added ? item.text : '' });
            }
        }
    });

    // Remove empty correction fields
    formattedDiff.forEach(item => {
        if (item.correction === '') {
            delete item.correction;
        }
    });

    return formattedDiff;
}
