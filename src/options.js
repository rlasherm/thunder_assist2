function saveOptions(e) {
    e.preventDefault();
    browser.storage.sync.set({
      endpoint: document.querySelector("#ai_endpoint").value
    });
  }
  
function restoreOptions() {
    function setCurrentChoice(result) {
      document.querySelector("#ai_endpoint").value = result.endpoint || "https://halfred.irisa.fr/completion";
    }
  
    function onError(error) {
      console.log(`Error: ${error}`);
    }
  
    let getting = browser.storage.sync.get("endpoint");
    getting.then(setCurrentChoice, onError);
  }
  
  document.addEventListener("DOMContentLoaded", restoreOptions);
  document.querySelector("form").addEventListener("submit", saveOptions);
  