function addButtonListeners() {
    var buttons = document.getElementsByClassName("toggle-option");

    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function() {
            // Retrieve the text from the clicked button
            const selectedText = this.innerText;

            // Find the parent toggle-container
            const toggleContainer = this.closest('.toggle-container');

            // Check for surrounding text and add spaces if needed
            let textToInsert = selectedText;
            if (toggleContainer.previousSibling && toggleContainer.previousSibling.nodeType === Node.TEXT_NODE && !toggleContainer.previousSibling.nodeValue.endsWith(' ')) {
                textToInsert = ' ' + textToInsert;
            }
            if (toggleContainer.nextSibling && toggleContainer.nextSibling.nodeType === Node.TEXT_NODE && !toggleContainer.nextSibling.nodeValue.startsWith(' ')) {
                textToInsert = textToInsert + ' ';
            }

            // Replace the toggle-container with the selected text
            if (toggleContainer) {
                toggleContainer.outerHTML = textToInsert;
            }
        });
    }
}

// Call the function to add listeners
addButtonListeners();
